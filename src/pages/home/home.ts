import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts1: any = [];
  constructor(public navCtrl: NavController) {

  }

  ionViewDidLoad() {
    this.menu();
  }

  menu() {
    
    this.posts1 = [
      { title: 'isuru', grad: 'Loran ipsum dummy text' },
      { title: 'SANKA', grad: 'grad' },
      { title: 'kamilas', grad: 'grad' },
      { title: 'sandeepa', grad: 'grad' }
    ];

  }

}
